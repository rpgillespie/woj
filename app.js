"use strict";
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var model = require('./model')

var app = express();

// Set up static dirs path
app.use(express.static(__dirname + '/public'));

//Setup POST body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))

// DB connection
mongoose.connect('mongodb://localhost/test');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  console.log('Connection to DB opened');
});

//Mongoose model
var questionModel = model.Question;

//helper prototype method
Array.prototype.shuffle = function() {
    var input = this;
     
    for (var i = input.length-1; i >=0; i--) {
     
        var randomIndex = Math.floor(Math.random()*(i+1));
        var itemAtIndex = input[randomIndex];
         
        input[randomIndex] = input[i];
        input[i] = itemAtIndex;
    }
    return input;
};

//Routes

//Default
app.get('/', function (req, res) {
  res.send('Welcome to Wheel of Jeorpardy Back end!');
});

//Questions
app.get('/questions',function(req, res){
  questionModel.find(function (err, questions) {
    if (err) return console.error(err);

    res.send(questions)
  });
});

//Categories
app.get('/categories', function(req, res){
  questionModel.distinct('category', function(err, categories){
    if (err) return console.error(err);

    res.send(categories);
  }); 
});

//Get documents with specified category id
app.get('/categories/:id',function(req, res){
  questionModel.find({'category' : req.params.id}, function (err, questions) {
    if (err) console.error(err);

    res.send(questions.shuffle().slice(0,6));
  });

});

//Post Mongo DB TODO
app.post('/home',function(req, res){
  res.send(req.body);
  console.log(req.body);

});

//Create new document in Mongo DB collection
app.post('/create',function(req, res){
  var q = new questionModel(req.body);

  q.save(function (err, q) {
    if (err) return console.error(err);

    console.log('Creating  API');
    console.log(req.body);
    res.send(req.body);

  });

});

//Update existing document in Mongo DB collection
app.post('/remove',function(req, res){
  questionModel.remove({ question : req.body.question }, function(err) {
    if (err) return console.error(err);

    console.log('Removing API');
    console.log(req.body);
    res.send(req.body);
  });
});

//Update existing document in Mongo DB collection
app.post('/update',function(req, res){
  questionModel.update({question : req.body.question}, { $set: { answer: req.body.answer, category :req.body.category, points : req.body.points }}, function(err){
    if (err) return console.error(err);

    console.log('Updating API');
    console.log(req.body);
    res.send(req.body);
  });
});

var server = app.listen(7777, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});

