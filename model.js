var mongoose = require('mongoose');

//DB schemas
var questionSchema = mongoose.Schema({
    question: String,
    answer: String,
    category: String,
    points: Number
});

// Model definition
module.exports.Question = mongoose.model('Question', questionSchema);

