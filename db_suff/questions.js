var questions = require('./qNaPair.json')
var model = require('./model') 

var questionModel = model.Question;

// DB connection
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  console.log('Connection to DB opened');
});


for (var i = 0; i < questions.length; i++){
  //console.log(questions[i]);
  var q = new questionModel(questions[i]);
  q.save(function (err, q) {
    if (err) return console.error(err);
  });
}

console.log(questions.length);
