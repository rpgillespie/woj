
var ROUND_SPINS = 50;

function mod(n, m) {
    return ((n % m) + m) % m;
}

sector2category = {
    0: 5,
    2: 4,
    4: 3,
    6: 2,
    8: 1,
    10: 0
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex ;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

function initBoard(round) {
    for (var i=0; i<categories.length; i++)
        for (var j=0; j<categories[i].length; j++)
        {   
            var category = categories[i][j];
            console.log(category)
            $.get("/categories/"+category, function(data) {

                var round = 1;
                if (board[0].length <= 5)
                    round = 0;

                for (var k=0; k<data.length; k++)
                {
                    data[k].points = (round+1)*(100*(k+1));
                }

                board[round].push(data);
            });
        }

    console.log(board);
}

function initCategories(round) {
    $.get("/categories", function(data) {
        var all_categories = shuffle(data);
        categories.push(all_categories.slice(0,6));  //Round 1
        categories.push(all_categories.slice(6,13)); //Round 2

        initBoard();
    });
}

function categoryHasQuestion(round, category) {
    var hasQuestion = false;
    for (var i=0; i<board[round][category].length; i++)
    {
        if (board[round][category][i].hide !== true)
            hasQuestion = true;
    }

    return hasQuestion;
}

var gameState = {

    start_game: true,
    returning_to_wheel: false,
    round: 0,
    round_spins: ROUND_SPINS,
    current_player: 0,

    wheel_strings: [
        "Category 6 Question",
        "Free Spin",
        "Category 5 Question",
        "Bankrupt",
        "Category 4 Question",
        "Opponent Choice",
        "Category 3 Question",
        "Lose Turn",
        "Category 2 Question",
        "Player's Choice",
        "Category 1 Question",
        "Spin Again"
    ],

    preload: function() {
        game.load.image('wheel', 'sprites/wheel.png');
        game.load.spritesheet('selecty', 'sprites/selecty.png');
        game.load.spritesheet('sbutton', 'sprites/spin_sheet.png', 190, 75);
        game.load.spritesheet('etbutton', 'sprites/endturn_sheet.png', 164, 66);
    },

    create: function() {

        if (this.start_game)
        {
            initCategories(this.round);
            this.start_game = false;
        }

        game.stage.backgroundColor = '#ffffff'

        this.wheel = game.add.sprite(game.world.centerX, game.world.centerY, 'wheel')
        this.wheel.anchor.setTo(.5, .5);

        this.selecty = game.add.sprite(game.world.centerX, game.world.centerY-260, 'selecty')
        this.selecty.anchor.setTo(.5, .5);

        this.sbutton = game.add.button(game.world.centerX+275, game.world.centerY+180, 'sbutton', this.spin, this, 1, 0, 2);
        this.sbutton.disabled = false
        this.sbutton.anchor.setTo(.5, .5);

        this.etbutton = game.add.button(game.world.centerX-265, game.world.centerY+180, 'etbutton', this.end_turn, this, 1, 0, 2);
        this.etbutton.disabled = true;
        this.etbutton.alpha = 0;
        this.etbutton.anchor.setTo(.5, .5);

        this.select_sector = game.add.text(game.world.centerX, game.world.centerY + 260, "", {font: "36pt Algerian", fill: '#000000'});
        this.select_sector.anchor.setTo(.5, .5);

        this.round_text = game.add.text(game.world.centerX + 200, game.world.centerY - 260, "Round "+(this.round+1), {font: "36pt Algerian", fill: '#000000'});
        this.round_text.anchor.setTo(.5, .5);
        this.remaining_spins = game.add.text(game.world.centerX + 200, game.world.centerY - 225, "Remaining Spins: "+(this.round_spins), {font: "14pt Ariel", fill: '#000000'});
        this.remaining_spins.anchor.setTo(.5, .5);

        game.add.text(0, 0, "Player", {font: "12pt Ariel", fill: '#000000'});
        game.add.text(70, 0, "Score", {font: "12pt Ariel", fill: '#000000'});
        game.add.text(140, 0, "Spins", {font: "12pt Ariel", fill: '#000000'});
        for (var i=0; i<players.length; i++)
        {
            players[i].name_text = game.add.text(0, 30 + i*30,  players[i].name, {font: "12pt Ariel", fill: '#000000'});
            players[i].score_text = game.add.text(70, 30 + i*30,  "$"+players[i].score, {font: "12pt Ariel", fill: '#000000'});
            players[i].spin_text = game.add.text(140, 30 + i*30,  ""+players[i].spins, {font: "12pt Ariel", fill: '#000000'});
        }

        if (!this.returning_to_wheel)
            this.start_turn();
        else
        {
            this.returning_to_wheel = false;
            this.etbutton.disabled = false;

            if (players[this.current_player].spins === 0)
            {
                this.end_turn();
            }
            else
            {
                this.etbutton.alpha = 1;
            }
                
        }
    },

    update: function() {

        this.update_scoreboard();

        if (this.spinning)
        {
            this.wheel.angle += 2;
            this.spindown--;

            var mod_angle = Math.floor(mod(this.wheel.angle, 360)),
                sector_angle = this.sector * 30;

            var current_sector = mod(Math.floor((mod_angle+15)/30), 12)
            this.select_sector.text = this.wheel_strings[current_sector];

            if (this.spindown <= 0 && Math.abs(sector_angle - mod_angle) < 3)
            {
                this.wheel.angle = sector_angle;
                this.spinning = false;

                var timer = game.time.create(false);
                timer.add(Phaser.Timer.SECOND * 1.5, this.determine_action, this);
                timer.start();
            }
        }
        else
        {
            if (players[this.current_player].spins > 0)
            {
                this.sbutton.disabled = false;
                this.sbutton.alpha = 1;
            }
        }

        if (this.sbutton.alpha > 0 && this.sbutton.disabled === true && players[this.current_player].spins === 0)
            this.sbutton.alpha -= .05;

        if (this.etbutton.alpha > 0 && this.etbutton.disabled === true)
            this.etbutton.alpha -= .05;
            
    },

    spin: function() {
        if (this.sbutton.disabled === true)
            return;

        this.round_spins--;
        this.remaining_spins.text = "Remaining Spins: "+(this.round_spins);

        players[this.current_player].spins--;

        this.sbutton.disabled = true;
        this.etbutton.disabled = true;

        this.sector = Math.floor(Math.random()*12);
        console.log(this.sector);

        this.spindown = 360; //At least 1 spins before settling on sector
        this.spinning = true;
    },

    start_turn: function() {
        players[this.current_player].spins++;
        this.etbutton.disabled = true;
        this.sbutton.disabled = false;
    },

    end_turn: function() {
        if (this.etbutton.disabled === true)
            return;

        this.current_player++;
        if (this.current_player >= players.length)
            this.current_player = 0;

        if (this.endOfRound())
        {
            if (this.round == 1)
                this.endGame();

            this.round++;
            this.round_text.text = "Round "+(this.round+1);
            this.round_spins = ROUND_SPINS;
            this.remaining_spins.text = "Remaining Spins: "+(this.round_spins);
        }

        this.start_turn();
    },

    endOfRound: function() {
        if (this.round_spins <= 0)
            return true;

        for (var i=0; i<6; i++)
            if (categoryHasQuestion(this.round, i))
                return false;

        return true;
    },

    endGame: function() {
        game.state.start("gameover");
    },

    determine_action: function() {

        if (this.sector % 2 === 0)
        {
            var category = sector2category[gameState.sector];

            if (categoryHasQuestion(this.round, category))
                game.state.start("board");
            else
            {
                players[this.current_player].spins++;
                this.select_sector.text = "Category Empty, Spin Again";
            }
        }

        //Free Spin
        if (this.sector === 1)
        {
            this.etbutton.disabled = false;
            this.etbutton.alpha = 1;
            players[this.current_player].spins++;
        }

        //Bankrupt
        if (this.sector === 3)
        {
            if (players[this.current_player].score > 0)
                players[this.current_player].score = 0;

            this.etbutton.disabled = false;
            this.end_turn();
        }

        //Player's Choice or Opponent's Choice
        if (this.sector === 5 || this.sector === 9)
            game.state.start("board");

        //Lose Turn
        if (this.sector === 7)
        {
            this.etbutton.disabled = false;
            this.end_turn();
        }

        //Spin Again
        if (this.sector === 11)
        {
            players[this.current_player].spins++;
        }
    },

    update_scoreboard: function() {
        for (var i=0; i<players.length; i++)
        {
            var color = '#000000';
            if (this.current_player === i)
                color = "#0000FF"

            players[i].name_text.fill = color;
            players[i].score_text.text = "$"+players[i].score;
            players[i].score_text.fill = color;
            players[i].spin_text.text = ""+players[i].spins;
            players[i].spin_text.fill = color;
        }
    }

};