var titleState = {

    preload: function() {
        game.load.image('wheel', 'sprites/wheel.png');
        game.load.spritesheet('sbutton', 'sprites/start.png', 164, 67);
    },

    create: function() {
        game.stage.backgroundColor = '#ffffff'
        this.wheel = game.add.sprite(game.world.centerX, game.world.centerY, 'wheel')
        this.wheel.anchor.setTo(.5, .5);

        var woj = game.add.text(game.world.centerX, game.world.centerY-260, 'Wheel of Jeopardy!', {font: "36pt Algerian", fill: '#000000'});
        woj.anchor.setTo(.5, .5);

        this.sbutton = game.add.button(game.world.centerX+275, game.world.centerY+180, 'sbutton', this.start, this, 1, 0, 2);
        this.sbutton.anchor.setTo(.5, .5);

        var numplayers = game.add.text(game.world.centerX, game.world.centerY+261, 'Number of Players:', {font: "24pt Ariel", fill: '#000000'});
        numplayers.anchor.setTo(.5, .5);

    },

    update: function() {
        this.wheel.angle -= .1;
    },

    start: function() {
        $('#quantity').hide();
        var numplayers = parseInt($('#quantity').val());

        if (numplayers > 15)
            numplayers = 15;

        if (numplayers < 1)
            numplayers = 1;

        players = []
        for (var i=0; i<numplayers; i++)
        {
            players.push({name: "Player " + (i+1), score: 0, spins: 0});
        }

        game.state.start("game");
    }

};