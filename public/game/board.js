//Converts X coordinate to a board column
function x2c(x) {
    return (x - 67) / 132;
}

//Converts Y coordinate to a board row
function y2r(y) {
    return (y - 215) / 85;
}

var boardState = {

    preload: function() {
        game.load.image('board_bg', 'sprites/board.png');
    },

    create: function() {
        this.background = game.add.tileSprite(0, 0, 800, 600, "board_bg");

        var category;
        var lock_category = false;

        if (gameState.sector !== 5 && gameState.sector !== 9)
        {
            category = sector2category[gameState.sector];
            lock_category = true;
        }

        //Draw Categories
        var font  = "10pt Arial",
            color = "#FFFFFF";

        if (lock_category)
            color = "#888888";

        for (var i=0; i<6; i++)
        {
            if (lock_category && i === category)
                this.draw_centered_text(70+i*133, 60, board[gameState.round][i][0].category, font, "#FFFFFF", false);
            else
                this.draw_centered_text(70+i*133, 60, board[gameState.round][i][0].category, font, color, false);

        }

        //Draw Question Grid
        font  = "24pt Calibri",
        color = "#FFCC00";

        var first,
            clickable = true;
        if (lock_category)
        {
            clickable = false;
            first = true;
            color = "#888888";
        }

        for (var i=0; i<5; i++)
        {
            for (var j=0; j<6; j++)
            {
                if (board[gameState.round][j][i].hide !== true)
                {
                    if (lock_category && j === category)
                    {
                        if (first === true)
                        {
                            this.draw_centered_text(67+j*132, 215+i*85, "$"+board[gameState.round][j][i].points, font, "#FFCC00", true);
                            first = false;
                        }
                        else
                            this.draw_centered_text(67+j*132, 215+i*85, "$"+board[gameState.round][j][i].points, font, color, clickable);    
                    }
                    else
                        this.draw_centered_text(67+j*132, 215+i*85, "$"+board[gameState.round][j][i].points, font, color, clickable);
                }
            }
        }
    },

    update: function() {
    },

    draw_centered_text: function(x, y, name, font, color, clickable) {
        var text = game.add.text(x, y, name, {font: font, fill: color});
        text.anchor.setTo(.5, .5);

        if (clickable) {
            text.inputEnabled = true;
            text.events.onInputUp.add(this.up, this);
            text.events.onInputOver.add(this.over, this);
            text.events.onInputOut.add(this.out, this);
        }
    },

    over: function(item) {
        item.fill = "#FF3300";
    },

    out: function(item) {
        item.fill = "#FFCC00";
    },

    up: function(item) {
        var category = x2c(item.x),
            question = y2r(item.y);

        questionState.question = board[gameState.round][category][question];
        board[gameState.round][category][question].hide = true;
        game.state.start("question");
    }
};