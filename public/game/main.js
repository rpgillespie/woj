
var game = new Phaser.Game(800, 600, Phaser.AUTO, "game-area");

//Each one holds 2 rounds
var categories = [[], []];
var board      = [[], []];
var players    = [
    {name: "Player1", spins: 0, score: 0},
    {name: "Player2", spins: 0, score: 0}
]

window.onload = function() {
    game.state.add("title", titleState);
    game.state.add("game", gameState);
    game.state.add("board", boardState);
    game.state.add("question", questionState);
    game.state.add("gameover", gameOverState);
    game.state.start("title");
};