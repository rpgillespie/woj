
function getMaxOfArray(numArray) {
  return Math.max.apply(null, numArray);
}

var gameOverState = {

    preload: function() {
    },

    create: function() {        
        this.game.stage.backgroundColor = '#000099';

        //Draw Categories
        var font  = "36pt Algerian",
            color = "#FFFFFF";

        this.gameover = game.add.text(game.world.centerX, game.world.centerY - 250, "Game Over", {font: font, fill: "#FF6666"});
        this.gameover.anchor.setTo(.5, .5);

        var txt1 = game.add.text(game.world.centerX - 75, game.world.centerY-200, "Player", {font: "24pt Ariel", fill: color});
        txt1.anchor.setTo(.5, .5);
        var txt2 = game.add.text(game.world.centerX + 75, game.world.centerY-200, "Score", {font: "24pt Ariel", fill: color});
        txt2.anchor.setTo(.5, .5);

        var scores = []
        for (var i=0; i<players.length; i++)
        {
            scores.push(players[i].score)

            var a = game.add.text(game.world.centerX - 75, game.world.centerY-150 + i*30,  players[i].name, {font: "24pt Ariel", fill: color});
            a.anchor.setTo(.5, .5);
            var b = game.add.text(game.world.centerX + 75, game.world.centerY-150 + i*30,  "$"+players[i].score, {font: "24pt Ariel", fill: color});
            b.anchor.setTo(.5, .5);
        }

        var max = getMaxOfArray(scores);
        console.log(max)

        for (var i=0; i<players.length; i++)
        {
            if (max === players[i].score)
            {
                var a = game.add.text(game.world.centerX, game.world.centerY+150 + i*40,  players[i].name+" wins!", {font: "24pt Ariel", fill: "#FFFF00"});
                a.anchor.setTo(.5, .5);      
            }
        }

    },

    update: function() {

    }


};