
var questionState = {

    question: {},

    preload: function() {
    },

    create: function() {
        this.submitted = false;
        this.no_time = false;
        $('#answer').val("");
        $('#answer').show().focus();
        
        this.game.stage.backgroundColor = '#3366CC';

        //Draw Categories
        var font  = "36pt Arial",
            color = "#FFFFFF";

        this.time = game.add.text(game.world.centerX, game.world.centerY - 250, 10, {font: font, fill: "#FF6666"});
        this.time.anchor.setTo(.5, .5);

        var text = game.add.text(game.world.centerX, game.world.centerY - 75, this.question.question, {font: font, fill: color});
        text.anchor.setTo(.5, .5);
        text.wordWrap = true;
        text.wordWrapWidth = 600;

        var key = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        key.onDown.add(this.submit, this);

        //Start Timer
        this.timer = game.time.create(false);

        //  Set a TimerEvent to occur after 100 seconds
        this.timer.add(Phaser.Timer.SECOND * 20, this.out_of_time, this);

        //  Start the timer running - this is important!
        //  It won't start automatically, allowing you to hook it to button events and the like.
        this.timer.start();
        //game.time.events.add(Phaser.Timer.SECOND * 10, this.out_of_time, this);
    },

    update: function() {
        if (!this.timer.paused)
            this.time.text = Math.ceil(this.timer.duration / 1000);
    },

    out_of_time: function(item) {
        $('#answer').hide();
        
        var font  = "36pt Arial",
            color = "#FF6666";

        var text = game.add.text(game.world.centerX, game.world.centerY + 150, "Out of time!\n+$0", {font: font, fill: color});
        text.anchor.setTo(.5, .5);

        console.log("Out of time!");

        this.transition_timer = game.time.create(false);
        this.transition_timer.add(Phaser.Timer.SECOND * 3, this.back_to_board, this);
        this.transition_timer.start();
    },

    submit: function(item) {
        this.timer.pause();

        $('#answer').hide();

        var font  = "36pt Arial";
        
        var answer = $('#answer').val().toLowerCase();

        this.transition_timer = game.time.create(false);
        if (answer === this.question.answer.toLowerCase())
        {
            players[gameState.current_player].score += this.question.points;
            var correct = game.add.text(game.world.centerX, game.world.centerY + 150, "Correct!\n+$"+this.question.points, {font: font, fill: "#00FF00"});
            correct.anchor.setTo(.5, .5);
            this.transition_timer.add(Phaser.Timer.SECOND * 2, this.back_to_board, this);
        }
        else
        {
            players[gameState.current_player].score -= this.question.points;
            var incorrect = game.add.text(game.world.centerX, game.world.centerY + 175, "Incorrect:\n'" + this.question.answer + "'\n-$"+this.question.points, {font: font, fill: "#FF6666"});
            incorrect.anchor.setTo(.5, .5);
            this.transition_timer.add(Phaser.Timer.SECOND * 5, this.back_to_board, this);
        }

        this.transition_timer.start();
    },

    back_to_board: function()
    {
        gameState.returning_to_wheel = true;
        game.state.start("game");
    }

};